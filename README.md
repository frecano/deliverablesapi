# README #

DeliverableAPI is a small app tha manages deadlines for deliverables.

### Entities

* Deliverables: A deliverable is a project to be delivered
* Actors: An actor is a person involved in the deliverable 
* Tasks: A task is part of the deliverable

### Definitions
* Task deadline: A deadline when the task is due, should not exceed the deadline of the deliverable
* Each deliverable consists of 2 tasks TaskA and TaskB
* The total deadline of the deliverable should consist of the deadlines of TaskA and TaskB

### How to run it? ###

You can use the default `sbt run` task, this downloads all the require dependencies and binds the server to your local 8080 port.

#### Test
Test can be run using the `sbt test` command


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Features currently supported
* Actor creation
* Fetch the full list of actors
* Create a deliverable defining the task A and B, and a list of actors 
* Mark a task as completed recording the current timestamp
* Mark a deliverable as completed which marks the uncompleted tasks of the deliverable as completed recording the timestamp.
* Fetch the list of all deliverables in the system
* Fetch a specific deliverable by id
* Delete a deliverable which also deletes its tasks


### Dependencies.
This application was used with the following libraries:
* Akka-http
* Slick
* h2database
