package com.gomedia
package model

import java.time.Instant
import org.scalatest.flatspec.AnyFlatSpec
import utils.DateFormatter

class DeliverableSpec extends AnyFlatSpec {
  trait Context {
    val maxDeadline = DateFormatter.dateFromString("2022-02-06")
    val deliverable = Deliverable(
      1,
      Task.fromDeadlineString("2022-02-04"),
      Task.fromDeadline(maxDeadline),
      Seq())
  }

  "A deliverable" should "has the totalDeadline as the max deadline among its tasks" in new Context {
    assert(deliverable.totalDeadline.compareTo(maxDeadline) == 0)
  }

  it should "be done when all its tasks are in done status" in new Context {
    val notDoneDeliverable = deliverable
    val doneDeliverable = notDoneDeliverable.copy(
      taskA = notDoneDeliverable.taskA.copy(status = TaskStatus.Done),
      taskB = notDoneDeliverable.taskB.copy(status = TaskStatus.Done)
    )
    assert(!notDoneDeliverable.isDone)
    assert(doneDeliverable.isDone)
  }

  it should "be on time when all its tasks where done before the totalDeadline" in new Context {
    val taskACompletedAt = Instant.parse("2022-01-31T15:19:59.347Z")
    val taskBCompletedAt = Instant.parse("2022-02-06T15:19:59.347Z")
    val overdueDate = Instant.parse("2022-02-07T15:19:59.347Z")
    val notDoneDeliverable = deliverable
    val onTimeDeliverable = deliverable.copy(
      taskA = deliverable.taskA.copy(status = TaskStatus.Done, completedAt = Option(taskACompletedAt)),
      taskB = deliverable.taskB.copy(status = TaskStatus.Done, completedAt = Option(taskBCompletedAt))
    )
    val overdueDeliverable = onTimeDeliverable.copy(
      taskA = onTimeDeliverable.taskA.copy(completedAt = Option(overdueDate)),
    )
    assert(onTimeDeliverable.onTime.contains(true))
    assert(overdueDeliverable.onTime.contains(false))
    assert(notDoneDeliverable.onTime.isEmpty)
  }

}
