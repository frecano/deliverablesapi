package com.gomedia
package service

import db.{ActorDAO, DeliverableDAO, TaskDAO}
import model.{Actor, Deliverable, Task, TaskStatus}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class DeliverableServiceSpec extends AnyFlatSpec with MockFactory {

  trait DeliverableCreationContext {
    val mockDeliverableDAO = mock[DeliverableDAO]
    val mockTaskDAO = mock[TaskDAO]
    val mockActorDAO = mock[ActorDAO]
    val service = new DeliverableService(mockDeliverableDAO, mockTaskDAO, mockActorDAO)
    val actors = Seq(Actor(1, "Bryan"))
    val deliverable = Deliverable(
      1,
      Task.fromDeadlineString("2022-02-04"),
      Task.fromDeadlineString("2022-02-04"),
      actors)

    def withActors(actors: Seq[Actor]) = {
      (mockActorDAO.get _).expects(Seq(1)).returning(Future.successful(actors)).once()
    }
  }

  trait ExistingDeliverableContext extends DeliverableCreationContext {
    def withExistingDeliverable(deliverable: Deliverable = deliverable) = {
      (mockDeliverableDAO.loadDerivable _).expects(1).returning(Future.successful(deliverable)).once()
    }

    def markingTaskAsCompleted(task: Task) = {
      (mockTaskDAO.markAsCompleted _).expects(task).returning(Future.successful(1)).once()
    }
  }

  "Deliverable service" should "not create a deliverable without tasks A or B" in new DeliverableCreationContext {
    withActors(actors)
    val deliverableWithMissingTask = deliverable.copy(taskA = null)
    val exception = intercept[Exception] {
      Await.result(service.storeNew(deliverableWithMissingTask), 5.seconds)
    }
    assert(exception.getMessage == "Tasks can't be null.")
  }

  it should "not create a deliverable when the actors do not exist in DB already" in new DeliverableCreationContext {
    withActors(Seq.empty)
    val exception = intercept[Exception] {
      Await.result(service.storeNew(deliverable), 5.seconds)
    }
    assert(exception.getMessage == "Missing actor(s) with id(s) 1")
  }

  it should "create a deliverable" in new DeliverableCreationContext {
    withActors(actors)
    (mockDeliverableDAO.create _).expects(deliverable, actors).returning(Future.unit).once()
    Await.result(service.storeNew(deliverable), 5.seconds)
  }

  it should "mark as complete a selected task" in new ExistingDeliverableContext {
    withExistingDeliverable()
    markingTaskAsCompleted(deliverable.taskB)
    Await.result(service.markTaskAsCompleted(1, "B"), 5.seconds)
  }

  it should "throw an error when trying to complete an already completed task" in new ExistingDeliverableContext {
    val deliverableWithTaskBCompleted = deliverable.copy(taskB = deliverable.taskB.copy(status = TaskStatus.Done))
    withExistingDeliverable(deliverableWithTaskBCompleted)
    val exception = intercept[Exception] {
      Await.result(service.markTaskAsCompleted(1, "B"), 5.seconds)
    }
    assert(exception.getMessage == "Cannot complete an already completed task")
  }

  it should "mark as completed a whole deliverable" in new ExistingDeliverableContext {
    withExistingDeliverable()
    markingTaskAsCompleted(deliverable.taskA)
    markingTaskAsCompleted(deliverable.taskB)
    Await.result(service.markAsCompleted(1), 5.seconds)
  }

  it should "throw an error when trying to complete an already completed deliverable" in new ExistingDeliverableContext {
    val completedDeliverable = deliverable.copy(
      taskA = deliverable.taskA.copy(status = TaskStatus.Done),
      taskB = deliverable.taskB.copy(status = TaskStatus.Done)
    )
    withExistingDeliverable(completedDeliverable)
    val error = intercept[Exception] {
      Await.result(service.markAsCompleted(1), 5.seconds)
    }
    assert(error.getMessage == "Deliverable already completed")
  }

  it should "delete a deliverable" in new ExistingDeliverableContext {
    withExistingDeliverable()
    (mockDeliverableDAO.delete _).expects(deliverable).returning(Future.unit).once()
    Await.result(service.delete(1), 5.seconds)
  }

}
