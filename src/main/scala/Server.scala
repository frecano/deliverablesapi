package com.gomedia

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import controller.marshallers.Marshallers
import db.DBSchema
import route.Routes
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps
import service.{ActorService, DeliverableService}

object Server extends App with Marshallers {

  implicit val system = ActorSystem(Behaviors.empty, "gomedia-system")
  implicit val executionContext = system.executionContext
  val (taskDAO, deliverableDAO, actorDAO) = new DBSchema()(executionContext).createDatabase
  val deliverableService = new DeliverableService(deliverableDAO, taskDAO, actorDAO)(executionContext)
  val actorService = new ActorService(actorDAO)(executionContext)

  scala.sys.addShutdownHook(() -> shutdown())

  Http().newServerAt("localhost", 8080).bind(Routes.routes)

  def shutdown(): Unit = {
    system.terminate()
    Await.result(system.whenTerminated, 30 seconds)
  }
}
