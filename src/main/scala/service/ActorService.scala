package com.gomedia
package service

import db.ActorDAO
import model.Actor
import scala.concurrent.{ExecutionContext, Future}

class ActorService(actorDAO: ActorDAO)(implicit ec: ExecutionContext) {

  def getAll(): Future[Seq[Actor]] = actorDAO.getAll

  def get(id: Int): Future[Actor] = actorDAO.getById(id)

  def storeNew(name: String) = {
    actorDAO.create(Actor(-1, name))
  }
}
