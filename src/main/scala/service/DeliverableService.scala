package com.gomedia
package service

import db.{ActorDAO, DeliverableDAO, TaskDAO}
import model.{Actor, Deliverable, Task, TaskStatus}
import scala.concurrent.{ExecutionContext, Future}

class DeliverableService(deliverableDAO: DeliverableDAO, taskDAO: TaskDAO, actorDAO: ActorDAO)(implicit ec: ExecutionContext) {

  def getAll(): Future[Seq[Deliverable]] = deliverableDAO.getAll

  def loadDeliverable(id: Int) = {
    deliverableDAO.loadDerivable(id)
  }

  def storeNew(newDeliverable: Deliverable) = {
    def validateInput(actors: Seq[Actor]) = {
      def checkForMissingActors() = {
        if (actors.size == newDeliverable.actors.size) {
          Future.unit
        } else {
          val missingActors = newDeliverable.actors.filterNot(a => actors.exists(_.id == a.id))
          Future.failed(new Exception(s"Missing actor(s) with id(s) ${missingActors.map(_.id).mkString(",")}"))
        }
      }

      def checkMissingTasks() = {
        if (newDeliverable.taskA == null || newDeliverable.taskB == null) {
          Future.failed(new Exception(s"Tasks can't be null."))
        } else {
          Future.unit
        }
      }

      for {
        () <- checkForMissingActors()
        () <- checkMissingTasks()
      } yield ()
    }

    for {
      actors <- actorDAO.get(newDeliverable.actors.map(_.id))
      () <- validateInput(actors)
      _ <- deliverableDAO.create(newDeliverable, actors)
    } yield ()
  }

  def markTaskAsCompleted(deliverableId: Int, taskSelected: String) = {
    def selectTask(deliverable: Deliverable) = {
      taskSelected match {
        case "A" => Future.successful(deliverable.taskA)
        case "B" => Future.successful(deliverable.taskB)
        case _ => Future.failed(new Exception("task selection not valid."))
      }
    }

    def checkValidStatus(task: Task) = {
      if (task.status != TaskStatus.Done) {
        Future.unit
      } else {
        Future.failed(new Exception(s"Cannot complete an already completed task"))
      }
    }

    for {
      deliverable <- deliverableDAO.loadDerivable(deliverableId)
      task <- selectTask(deliverable)
      () <- checkValidStatus(task)
      _ <- taskDAO.markAsCompleted(task)
    } yield ()
  }

  def markAsCompleted(deliverableId: Int) = {
    def checkValidStatus(deliverable: Deliverable) = {
      if (deliverable.tasks.forall(_.status == TaskStatus.Done)) {
        Future.failed(new Exception("Deliverable already completed"))
      } else {
        Future.unit
      }
    }

    for {
      deliverable <- deliverableDAO.loadDerivable(deliverableId)
      () <- checkValidStatus(deliverable)
      _ <- Future.sequence(deliverable.tasks
        .filterNot(_.status == TaskStatus.Done)
        .map(taskDAO.markAsCompleted))
    } yield ()
  }

  def delete(deliverableId: Int) = {
    for {
      deliverable <- deliverableDAO.loadDerivable(deliverableId)
      () <- deliverableDAO.delete(deliverable)
    } yield ()
  }

}
