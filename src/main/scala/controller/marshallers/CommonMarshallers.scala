package com.gomedia
package controller.marshallers

import java.time.Instant
import java.util.Date
import scala.util.Try
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, deserializationError}
import utils.DateFormatter

trait CommonMarshallers extends DefaultJsonProtocol {

  implicit object DateFormat extends JsonFormat[Date] {
    def write(date: Date) = JsString(DateFormatter.dateToString(date))
    def read(json: JsValue) = json match {
      case JsString(rawDate) =>
        Try {
          DateFormatter.dateFromString(rawDate)
        }.toOption
          .fold(deserializationError(s"Expected ISO Date format, got $rawDate"))(identity)
      case error => deserializationError(s"Expected JsString, got $error")
    }
  }

  implicit object InstantFormat extends JsonFormat[Instant] {
    def write(instant: Instant) = JsString(instant.toString)
    def read(json: JsValue) = json match {
      case JsString(rawInstant) => Try { Instant.parse(rawInstant) }
        .toOption
        .fold(deserializationError(s"Expected Instant format, got $rawInstant"))(identity)
      case error => deserializationError(s"Expected JsString, got $error")
    }
  }


}
