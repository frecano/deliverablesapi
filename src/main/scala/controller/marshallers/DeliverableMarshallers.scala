package com.gomedia
package controller.marshallers

import model.{Actor, Deliverable, Task}
import scala.util.Try
import spray.json._

trait DeliverableMarshallers extends DefaultJsonProtocol with CommonMarshallers with TaskMarshallers {

  implicit val actorFormat = jsonFormat2(Actor)
  implicit val deliverableFormat = new RootJsonFormat[Deliverable]{
    override def read(json: JsValue): Deliverable = json.asJsObject
      .getFields("taskA", "taskB", "actors") match {
      case Seq(taskAJson, taskBJson, JsArray(actorsJson)) => Try {
          val taskA = taskAJson.convertTo[Task]
          val taskB = taskBJson.convertTo[Task]
          val actors = actorsJson.map(_.asJsObject.getFields("id") match {
            case Seq(JsNumber(actorId)) => Actor(actorId.toInt, "")
            case param => throw DeserializationException(s"Invalid actor id $param")
          })
          Deliverable(-1, taskA, taskB, actors)
        }.toOption
        .fold(deserializationError(s"Expected Deliverable, got ${json.prettyPrint}"))(identity)
      case error => deserializationError(s"Expected JsString, got $error")
    }

    override def write(deliverable: Deliverable): JsValue = {
      JsObject(
        "id" -> JsNumber(deliverable.id),
        "taskA" -> deliverable.taskA.toJson,
        "taskB" -> deliverable.taskB.toJson,
        "actors" -> deliverable.actors.toJson,
        "onTime" -> deliverable.onTime.map(x => JsBoolean(x)).getOrElse(JsNull)
      )
    }
  }

}