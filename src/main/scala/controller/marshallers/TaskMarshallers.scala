package com.gomedia
package controller.marshallers

import model.TaskStatus.TaskStatus
import model.{Task, TaskStatus}
import scala.util.Try
import spray.json._

trait TaskMarshallers extends CommonMarshallers {

  implicit object taskStatusFormat extends JsonFormat[TaskStatus] {

    override def read(json: JsValue) = json match {
      case JsString(rawStatus) => Try { TaskStatus.withName(rawStatus.toLowerCase) }
        .toOption
        .fold(deserializationError(s"Expected TaskStatus, got $rawStatus"))(identity)
      case error => deserializationError(s"Expected TaskStatus, got $error")
    }

    override def write(status: TaskStatus) = JsString(status.toString)

  }

  implicit object TaskFormat extends JsonFormat[Task] with CommonMarshallers {

    override def read(json: JsValue) = json.asJsObject
      .getFields("deadline") match {
        case Seq(JsString(rawDeadline)) => Try {
            Task.fromDeadlineString(rawDeadline)
          }.toOption
          .fold(deserializationError(s"Expected Task, got ${json.prettyPrint}"))(identity)
        case error => deserializationError(s"Expected JsString, got $error")
    }

    override def write(task: Task) = JsObject(
      "id" -> JsNumber(task.id),
      "deadline" -> task.deadline.toJson,
      "status" -> task.status.toJson,
      "completed_at" -> task.completedAt.map(_.toJson).getOrElse(JsNull)
    )
  }
}
