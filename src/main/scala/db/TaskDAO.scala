package com.gomedia
package db

import com.gomedia.model.{Deliverable, Task, TaskStatus}
import java.time.Instant
import scala.concurrent.Future
import slick.jdbc.H2Profile.api._

class TaskDAO(db: Database) extends TasksTable {
  def create(deliverable: Deliverable) = {
    tasks ++= Seq(deliverable.taskA, deliverable.taskB)
  }

  def markAsCompleted(task: Task) = {
    val action = tasks
      .filter(_.id === task.id)
      .map(row => (row.status, row.completedAt))
      .update(TaskStatus.Done, Option(Instant.now))
    db.run(action)
  }
}

