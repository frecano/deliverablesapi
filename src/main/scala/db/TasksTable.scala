package com.gomedia
package db

import java.time.Instant
import java.util.Date
import model.TaskStatus.TaskStatus
import model.{Task, TaskStatus}
import scala.language.postfixOps
import slick.jdbc.H2Profile.api._
import utils.DateFormatter

trait TasksTable {

  val tasks = TableQuery[Tasks]

  implicit val taskStatusColumnType = MappedColumnType.base[TaskStatus, String](
    { status => status.toString },
    { rawStatus => TaskStatus.withName(rawStatus)}
  )

  implicit val dateColumnType = MappedColumnType.base[Date, String](
    { date => DateFormatter.dateToString(date) },
    { rawDate => DateFormatter.dateFromString(rawDate)}
  )

  class Tasks(tag: Tag) extends Table[Task](tag, "tasks") {

    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def deliverableId = column[Int]("deliverable_id")
    def deadline = column[Date]("deadline")
    def status = column[TaskStatus]("status")
    def completedAt = column[Option[Instant]]("completed_at")

    def * = (id, deadline, status, completedAt, deliverableId) <> ((Task.apply _).tupled, Task.unapply)
  }
}
