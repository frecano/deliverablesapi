package com.gomedia
package db

import model.Actor
import scala.language.postfixOps
import slick.jdbc.H2Profile.api._

trait ActorsTable {

  val actors = TableQuery[Actors]

  private val insertActor = actors returning actors.map(_.id) into ((item, id) => item.copy(id = id))

  def createActor(actor: Actor): DBIO[Actor] = {
    insertActor += actor
  }

  class Actors(tag: Tag) extends Table[Actor](tag, "actors") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")

    def * = (id, name).mapTo[Actor]
  }
}
