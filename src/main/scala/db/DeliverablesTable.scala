package com.gomedia
package db

import model.{Deliverable, DeliverableDB}
import scala.language.postfixOps
import slick.jdbc.H2Profile.api._

trait DeliverablesTable {

  class Deliverables(tag: Tag) extends Table[DeliverableDB](tag, "deliverables") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def * = (id).mapTo[DeliverableDB]
  }
}
