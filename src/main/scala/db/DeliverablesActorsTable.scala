package com.gomedia
package db

import model.{Actor, DeliverablesActor}
import scala.language.postfixOps
import slick.jdbc.H2Profile.api._

trait DeliverablesActorsTable {

  val deliverablesActors = TableQuery[DeliverablesActors]

  def insertDeliverableActor(deliverableId: Int, actorId: Int) = {
    deliverablesActors += DeliverablesActor(-1, deliverableId, actorId)
  }

  class DeliverablesActors(tag: Tag) extends Table[DeliverablesActor](tag, "deliverables_actors") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def deliverableId = column[Int]("deliverable_id")
    def actorId = column[Int]("actor_id")

    def * = (id, deliverableId, actorId).mapTo[DeliverablesActor]
  }
}
