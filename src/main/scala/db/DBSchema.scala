package com.gomedia
package db

import model.{Actor, Deliverable, Task, TaskStatus}
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}
import scala.language.postfixOps
import slick.jdbc.H2Profile.api._
import utils.DateFormatter

class DBSchema()(implicit executionContext: ExecutionContext) {
  lazy val db = Database.forConfig("h2mem")
  lazy val taskDAO: TaskDAO = new TaskDAO(db)
  lazy val deliverableDAO = new DeliverableDAO(db)
  lazy val actorDAO = new ActorDAO(db)
  lazy val deliverablesActorsTable = new DeliverablesActorsTable {}

  def createDatabase = {
    databaseSetup()
    (taskDAO, deliverableDAO, actorDAO)
  }

  private def databaseSetup() = {
    val createSchema = DBIO.seq(
      taskDAO.tasks.schema.create,
      deliverableDAO.deliverables.schema.create,
      actorDAO.actors.schema.create,
      deliverablesActorsTable.deliverablesActors.schema.create)

    Await.result(db.run(createSchema), 10 seconds)
    Await.result(db.run(prePopulateDB), 10 seconds)
  }

  private def prePopulateDB = {
    val actor1 = Actor(1, "John Doe")
    val actor2 = Actor(2, "Carlos")
    val actors = Seq(actor1, actor2)
    val deliverable = Deliverable(1,
      Task(1, deadline = DateFormatter.dateFromString("2021-01-13"), TaskStatus.ToDo, None, 1),
      Task(2, deadline = DateFormatter.dateFromString("2021-02-06"), TaskStatus.ToDo, None, 1),
      actors = actors)
    DBIO.seq(
      actorDAO.actors.forceInsertAll(actors),
      DBIO.from(deliverableDAO.create(deliverable, actors))
    )
  }

}
