package com.gomedia
package db

import model.Actor
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.H2Profile.api._

class ActorDAO(db: Database)(implicit executionContext: ExecutionContext) extends ActorsTable {
  private val insertQuery = actors returning actors.map(_.id) into ((item, id) => item.copy(id = id))

  def getAll() = db.run(actors.result)

  def getById(id: Int): Future[Actor] = {
    db.run(actors.filter(_.id === id)
      .result.headOption.map(_.getOrElse(throw new Exception("Actor with id $id not found."))))
  }

  def get(ids: Seq[Int]): Future[Seq[Actor]] = {
    db.run(actors.filter(_.id inSet ids).result)
  }

  def create(actor: Actor): Future[Actor] = {
    db.run(insertQuery += actor)
  }

  def delete(actor: Actor) = {
    db.run(actors.filter(_.id === actor.id).delete)
  }
}

