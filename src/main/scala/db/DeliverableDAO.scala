package com.gomedia
package db

import model.{Actor, Deliverable, DeliverableDB}
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.H2Profile.api._

class DeliverableDAO(db: Database)(implicit executionContext: ExecutionContext)
  extends DeliverablesTable with TasksTable with ActorsTable with DeliverablesActorsTable {
  val deliverables = TableQuery[Deliverables]
  private val insertQuery = deliverables returning deliverables.map(_.id) into ((item, id) => item.copy(id = id))

  def getAll: Future[Seq[Deliverable]] = {
    val allDerivablesDBFuture = db.run(deliverables.result)
    allDerivablesDBFuture.flatMap { deliverableDBs =>
      Future.sequence(deliverableDBs
        .map(d => loadDerivable(d.id)))
    }
  }

  def getDerivable(id: Int) = {
    deliverables.filter(_.id === id).result.headOption.map(_.getOrElse(throw new Exception(s"Not found deliverable with id $id.")))
  }

  def loadDerivable(id: Int) = {
    val tx  = for {
      deliverableDB <- getDerivable(id)
      tasks <- tasks.filter(_.deliverableId === deliverableDB.id).sortBy(_.id).result
      actors <- deliverablesActors.join(actors)
        .on(_.actorId ===_.id)
        .filter(_._1.deliverableId === deliverableDB.id).map(_._2).result
    } yield Deliverable(deliverableDB.id, tasks(0), tasks(1), actors) //TODO make tasks a list and the task A and B attributes
    db.run(tx)
  }

  def createNew(): DBIO[DeliverableDB] = {
    insertQuery  += DeliverableDB(0)
  }

  def create(newDeliverable: Deliverable, actors: Seq[Actor]) = {
    val tx = for {
      deliverableDb <- createNew()
      deliverableWithId = newDeliverable.setDeliverableId(deliverableDb.id)
      _ <- (tasks ++= Seq(deliverableWithId.taskA, deliverableWithId.taskB))
      _ <- DBIO.sequence(actors.map(a => insertDeliverableActor(deliverableDb.id, a.id)))
    } yield ()
    db.run(tx.transactionally)
  }

  def delete(deliverable: Deliverable) = {
    val tx = for {
      _ <- tasks.filter(_.id inSet deliverable.tasks.map(_.id)).delete
      _ <- deliverables.filter(_.id === deliverable.id).delete
    } yield ()

    db.run(tx.transactionally)
  }
}

