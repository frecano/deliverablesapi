package com.gomedia
package route

import akka.http.scaladsl.server.Directives._

object Routes extends DeliverableRoutes with ActorRoutes {

  val routes = concat(
    path("deliverables")(deliverablesRoutes),
    pathPrefix("deliverable" / IntNumber)(deliverableRoute),
    path("actors")(actorsRoutes))
}
