package com.gomedia
package route

import Server.deliverableService
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives._
import controller.marshallers.Marshallers
import model.Deliverable
import scala.util.{Failure, Success}

trait DeliverableRoutes extends Marshallers {
  def deliverablesRoutes() = {
    get {
      val deliverablesFuture = deliverableService.getAll()
      onComplete(deliverablesFuture) {
        case Success(deliverables) => complete(deliverables)
        case Failure(error) => complete(error.getMessage)
      }
    } ~
      post {
        entity(as[Deliverable]) { deliverable =>
          val saved = deliverableService.storeNew(deliverable)
          onComplete(saved) {
            case Success(_) => complete("Deliverable created")
            case Failure(error) => complete(BadRequest, error.getMessage)
          }
        }
      }
  }

  def deliverableRoute(deliverableId: Int) = {
    concat(
      get {
        pathEnd {
          val deliverableFuture = deliverableService.loadDeliverable(deliverableId)
          onComplete(deliverableFuture) {
            case Success(deliverable) => complete(deliverable)
            case Failure(error) => complete(BadRequest, error.getMessage)
          }
        }
      },
      delete {
        pathEnd {
          val deleted = deliverableService.delete(deliverableId)
          onComplete(deleted) {
            case Success(_) => complete(s"Deliverable $deliverableId deleted.")
            case Failure(error) => complete(BadRequest, error.getMessage)
          }
        }
      },
      patch {
        concat(
          path("complete") {
            val marked = deliverableService.markAsCompleted(deliverableId)
            onComplete(marked) {
              case Success(_) => complete("Deliverable marked as completed")
              case Failure(error) => complete(BadRequest, error.getMessage)
            }
          },
          pathPrefix("task" / Segment) { taskSelected =>
            concat(
              path("complete") {
                val marked = deliverableService.markTaskAsCompleted(deliverableId, taskSelected)
                onComplete(marked) {
                  case Success(_) => complete("task marked as completed")
                  case Failure(error) => complete(BadRequest, error.getMessage)
                }
              }
            )
          })
      }
    )
  }
}
