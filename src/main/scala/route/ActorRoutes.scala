package com.gomedia
package route

import Server.actorService
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives._
import controller.marshallers.Marshallers
import scala.util.{Failure, Success}

trait ActorRoutes extends Marshallers {
  def actorsRoutes() = {
    get {
      val actorsFuture = actorService.getAll()
      onComplete(actorsFuture) {
        case Success(actors) => complete(actors)
        case Failure(error) => complete(error.getMessage)
      }
    } ~
      post {
        entity(as[Map[String, String]]) { body =>
          body.get("name") match {
            case None => complete(BadRequest, "Missing actor name")
            case Some(name) =>
              val saved = actorService.storeNew(name)
              onComplete(saved) {
                case Success(_) => complete("Actor created")
                case Failure(error) => complete(BadRequest, error.getMessage)
              }
          }
        }
      }
  }
}


