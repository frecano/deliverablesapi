package com.gomedia
package model

import java.time.Instant
import java.util.Date
import model.TaskStatus.TaskStatus
import utils.DateFormatter

object TaskStatus extends Enumeration {
  type TaskStatus = Value
  val Unknown = Value("unknown")
  val ToDo = Value("todo")
  val InProgress = Value("in_progress")
  val Done = Value("done")
}

final case class Task(id: Int, deadline: Date, status: TaskStatus, completedAt: Option[Instant], deliverableId: Int) {
  def setDeliverable(deliverableId: Int) = {
    this.copy(deliverableId = deliverableId)
  }

  def setStatus(newStatus: TaskStatus): Task = {
    this.copy(status = newStatus)
  }
}

object Task {
  def fromDeadline(deadline: Date) = {
    Task(-1, deadline, TaskStatus.ToDo, None, -1)
  }

  def fromDeadlineString(rawDeadline: String) = {
    val deadline = DateFormatter.dateFromString(rawDeadline)
    fromDeadline(deadline)
  }
}
