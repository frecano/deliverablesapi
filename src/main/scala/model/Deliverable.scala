package com.gomedia
package model

import java.time.temporal.ChronoUnit

final case class Deliverable(id: Int, taskA: Task, taskB: Task, actors: Seq[Actor]) {

  def setDeliverableId(deliverableId: Int) = {
    val updatedTaskA = taskA.setDeliverable(deliverableId)
    val updatedTaskB = taskB.setDeliverable(deliverableId)
    this.copy(id = deliverableId, taskA = updatedTaskA, taskB = updatedTaskB)
  }

  def totalDeadline = Seq(taskA.deadline, taskB.deadline).max
  def tasks = Seq(taskA, taskB)
  def isDone = tasks.forall(_.status == TaskStatus.Done)
  def onTime: Option[Boolean] = {
    if (isDone) {
      Option(tasks.forall(_.completedAt
        .forall(_.truncatedTo(ChronoUnit.DAYS)
          .compareTo(totalDeadline.toInstant) <= 0)))
    } else {
      None
    }
  }

}

final case class DeliverableDB(id: Int)

final case class DeliverablesActor(id: Int, deliverableId: Int, actorId: Int)