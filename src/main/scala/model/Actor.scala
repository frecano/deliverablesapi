package com.gomedia
package model

final case class Actor(id: Int, name: String)
