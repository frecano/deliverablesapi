package com.gomedia
package utils

import java.text.SimpleDateFormat
import java.util.Date

object DateFormatter {

  private val localIsoDateFormatter = new ThreadLocal[SimpleDateFormat] {
    override def initialValue() = new SimpleDateFormat("yyyy-MM-dd")
  }

  def dateToString(date: Date) = localIsoDateFormatter.get().format(date)

  def dateFromString(rawDate: String): Date = localIsoDateFormatter.get().parse(rawDate)

}
